# Home automation v1

> Deprecated: see [home-automation-v2](https://gitlab.com/danhawkes/home-automation-v2)

<div align="center">
<img src=".docs/board.jpg" width="668">
</div>

## Hardware

* Raspberry Pi Zero W
* Nexa 433 MHz mains switches
  ([clas ohlson](https://www.clasohlson.com/uk/Nexa-Remote-Switch-Set-3-pack/18-2650))
* 433Mhz transmitter/receiver boards (MX-05V, MX-FS-03V, ebay)
* DC boost converter (for transmitter) (LM2577-alike, ebay)

## Project structure

* `api` - REST API server for the nexa binary
* `caddy` - ARMv6 build of caddy proxy
* `compose` - Docker-compose config for deployment of web/api
* `hypriotos` - Device init script to start the above compose config on boot
* `nexa` - Command-line utility to send commands to nexa receivers
* `node-image` - ARMv6 build of node, with the qemu binary injected so it will
  run on x86_64
* `web` - Web frontend

## Building/deployment

The various components are set up for development on x86, and deployment to
ARMv6. The reason for this is that development on the pi is cumbersome (with
lack of dev tooling), and painfully slow (e.g. ~10 mins for a yarn install).

To support ARM compilation on x86, the excellent
[qemu-user-static](https://github.com/multiarch/qemu-user-static) and
[crossbuild](https://github.com/multiarch/crossbuild) projects are used.

To build/deploy the production ARM components, use the makefile in the root
directory:

    make {nexa,api,web,caddy}
    make deploy-{nexa,api,web,caddy,compose}

Component-specific instructions for local development are listed below.

### API

Build/run locally, with a mock implementation of the nexa binary:

    yarn dev

The API server runs on port `3001` by default. This allows interop with the
`create-react-app` web component.

### Web

Build/run locally:

    yarn dev

The web server runs on port `3000` by default, and proxies requests it can't
handle to the API server at `localhost:3001`.

### Nexa

Requires `build-essential` (or equivalent), `autoconf`, and `automake` for local
development.

Running `make` will generate the nexa binary.

Note that make recursively builds the `wiringPi` and `yuck` dependencies.
