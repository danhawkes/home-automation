#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/param.h>
#include <wiringPi.h>

#include "tx.h"

static const int PIN = 2;
static const int WIRE_TIMEUNIT = 250;
static const int SENDS_IN_PACKET = 5;

void l1() {
  digitalWrite(PIN, HIGH);
  delayMicroseconds(WIRE_TIMEUNIT);
  digitalWrite(PIN, LOW);
  delayMicroseconds(WIRE_TIMEUNIT);
}

void l0() {
  digitalWrite(PIN, HIGH);
  delayMicroseconds(WIRE_TIMEUNIT);
  digitalWrite(PIN, LOW);
  delayMicroseconds(WIRE_TIMEUNIT * 5);
}

void wire(_Bool bit) {
  if (bit) {
    l1();
    l0();
  } else {
    l0();
    l1();
  }
}

void wireSync() {
  digitalWrite(PIN, HIGH);
  delayMicroseconds(WIRE_TIMEUNIT);
  digitalWrite(PIN, LOW);
  delayMicroseconds(WIRE_TIMEUNIT * 10);
}

void wirePause() {
  digitalWrite(PIN, HIGH);
  delayMicroseconds(WIRE_TIMEUNIT);
  digitalWrite(PIN, LOW);
  delayMicroseconds(WIRE_TIMEUNIT * 40);
}

void wireData(uint8_t *data, int len) {
  int bytes = (len / 8) + ((len % 8 == 0) ? 0 : 1);

  for (int i = 0; i < bytes; i++) {
    int bits = MIN(len - (i * 8), 8);
    uint8_t byte = data[i];

    for (int j = 7; j >= (8 - bits); j--) {
      wire(byte & (1 << j));
    }
  }
}

void setupTx() {
  wiringPiSetup();
  pinMode(PIN, OUTPUT);
}

void wirePacket(uint8_t *data, int len) {
  for (int i = 0; i < SENDS_IN_PACKET; i++) {
    wireSync();
    wireData(data, len);
    wirePause();
  }
}
