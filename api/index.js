const PROD = process.env.NODE_ENV === 'production';
const PORT = process.env.PORT || 3001;

const express = require('express');
const Nexa = require('./Nexa');

const redux = require('redux');
const actions = require('./actions');
const rootReducer = require('./reducers');

const store = redux.createStore(rootReducer);

const app = express();

app.use(express.json());

// Merge query and body params into data object
app.use((req, res, next) => {
  let data = {};
  if (req.body) {
    data = { ...data, ...req.body };
  }
  if (req.query) {
    data = { ...data, ...req.query };
  }
  req.data = data;
  next();
});

// Get state of all switches switch
app.get('/api/switches', (req, res) => {
  res.status(200).json(store.getState().switches);
});

// Get state of a single switch
app.get('/api/switch', (req, res) => {
  let { id } = req.data;
  if (!id) {
    res.status(400).json({ error: 'Switch ID not specified' });
    return;
  }
  let switchData = store.getState().switches[id];
  if (!switchData) {
    res.status(404).json({ error: `Switch with ID '${id}' is not known` });
    return;
  }
  res.status(200).json(switchData);
});

// Set state of single switch
app.post('/api/switch', (req, res, next) => {
  let reqId = req.data.id;
  let reqOn = req.data.on;
  if (!reqId) {
    res.status(400).json({ error: 'Switch ID not specified' });
    return;
  }
  if (!reqOn) {
    res.status(400).json({ error: `Switch 'on' state not specified` });
    return;
  }

  let id = reqId;
  let on = reqOn === 'true' || reqOn === 1;
  let state = store.getState();
  let switchState = state.switches[id];

  if (!state.switches[id]) {
    res.send(400).json({ error: `Switch with ID '${id}' is not known` });
    return;
  }

  store.dispatch(actions.setSwitch(id, on));

  let nexaGroupCode = '1';
  let nexaChannelCode = '11';
  let data =
    switchState.serial +
    nexaGroupCode +
    (on ? '0' : '1') +
    nexaChannelCode +
    switchState.unit;

  console.log(
    `Setting switch '${switchState.name}' to ${on ? 'on' : 'off'}: ${data}`
  );

  Nexa.sendPacket(data).then(
    output => {
      res.status(200).end();
    },
    err => {
      store.dispatch(actions.setSwitch(id, !on));
      next(err);
    }
  );
});

app.use((err, req, res, next) => {
  res.status(500).json({ error: err.message });
});

app.listen(PORT);

console.log(
  `Started API server [mode: ${PROD ? 'prod' : 'dev'}, port: ${PORT}]`
);
