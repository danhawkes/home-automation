function setSwitch(id, on) {
  return {
    type: 'SET_SWITCH',
    id,
    on
  };
}

module.exports = {
  setSwitch
};
